package matrix

import (
	"fmt"
	"unicode/utf8"
)

type Matrix struct {
	rows, columns uint
	data []int
}

func MatrixNew(rows, columns uint) *Matrix {
	return &Matrix{rows: rows, columns: columns, data: make([]int, rows * columns)}
}

func (m *Matrix) At(row, column uint) int {
	return m.data[row * m.columns + column]
}

func (m *Matrix) AtPut(row, column uint, value int) {
	m.data[row * m.columns + column] = value
}

func MatrixNewInit(rows, columns uint, data []int) *Matrix {
	if uint(len(data)) != (rows * columns) {
		return nil
	}
	result := MatrixNew(rows, columns)
	var i, j uint
	for i = 0; i < rows; i++ {
		for j = 0; j < columns; j++ {
			result.AtPut(i, j, data[(i * columns) + j])
		}
	}
	return result
}

func MatrixNewIdentity(rc uint) *Matrix {
	result := MatrixNew(rc, rc)
	var i uint
	for i = 0; i < rc; i++ {
		result.AtPut(i, i, 1)
	}
	return result
}

func (m *Matrix) Copy() *Matrix {
	result := MatrixNew(m.rows, m.columns)
	var i, j uint
	for i = 0; i < m.rows; i++ {
		for j = 0; j < m.columns; j++ {
			result.AtPut(i, j, m.At(i, j))
		}
	}
	return result
}

func (m *Matrix) Transpose() *Matrix {
	result := MatrixNew(m.columns, m.rows)
	var i, j uint
	for i = 0; i < m.rows; i++ {
		for j = 0; j < m.columns; j++ {
			result.AtPut(j, i, m.At(i, j))
		}
	}
	return result
}

func (m *Matrix) Add(a *Matrix) *Matrix {
	if m.rows != a.rows || m.columns != a.columns {
		return nil
	}
	result := MatrixNew(m.rows, m.columns)
	var i, j uint
	for i = 0; i < m.rows; i++ {
		for j = 0; j < m.columns; j++ {
			result.AtPut(i, j, m.At(i, j) + a.At(i, j))
		}
	}
	return result
}

func (m *Matrix) Mult(a *Matrix) *Matrix {
	if m.columns != a.rows {
		return nil
	}
	result := MatrixNew(m.rows, a.columns)
	var i, j, k uint
	for i = 0; i < m.rows; i++ {
		for j = 0; j < a.columns; j++ {
			s := 0
			for k = 0; k < m.columns; k++ {
				s += m.At(i, k) * a.At(k, j)
			}
			result.AtPut(i, j, s)
		}
	}
	return result
}

func (m *Matrix) Reduce(row1, row2, column uint) *Matrix {
	result := m.Copy()
	var j uint
	for j = 0; j < m.columns; j++ {
		result.AtPut(row1, j, m.At(row1, column) * m.At(row2, j) - m.At(row2, column) * m.At(row1, j))
	}
	return result
}

func (m *Matrix) Gaussian() *Matrix {
	if m.columns != (m.rows + 1) {
		return nil
	}
	result := m.Copy()
	k := m.columns - 1
	var j uint
	for j = 0; j < k; j++ {
		var i uint
		for i = m.rows; i > (j + 1); {
			i--
			result = result.Reduce(i, i - 1, j)
		}
	}
	for j = k; j > 0; {
		j--
		var i uint
		for i = 0; i < j; i++ {
			result = result.Reduce(i, i + 1, j)
		}
	}
	return result
}

func (m *Matrix) Solve() *[]float64 {
	m = m.Gaussian()
	result := make([]float64, m.rows)
	k := m.columns - 1
	var i uint
	for i = 0; i < m.rows; i++ {
		result[i] = float64(m.At(i, k)) / float64(m.At(i, i))
	}
	return &result
}

func (m *Matrix) String() string {
	var result string
	var i, j uint
	b := make([]byte, 3)
	utf8.EncodeRune(b, 0x2502)
	vert := string(b)
	maxLen := 0
	for i = 0; i < m.rows; i++ {
		x := fmt.Sprintf("%v", m.At(i, j))
		n := len(x)
		if n > maxLen {
			maxLen = n
		}
	}
	maxLen++
	for i = 0; i < m.rows; i++ {
		switch i {
		case 0:
			utf8.EncodeRune(b, 0x250c)
			result += string(b)
		case m.rows - 1:
			result += "\n"
			utf8.EncodeRune(b, 0x2514)
			result += string(b)
		default:
			result += "\n"
			result += vert
		}
		for j = 0; j < m.columns; j++ {
			if j > 0 {
				result += " "
			}
			result += fmt.Sprintf("%*v", maxLen, m.At(i, j))
		}
		result += " "
		switch i {
		case 0:
			utf8.EncodeRune(b, 0x2510)
			result += string(b)
		case m.rows - 1:
			utf8.EncodeRune(b, 0x2518)
			result += string(b)
		default:
			result += vert
		}
	}
	return result
}
