package main

import (
	"fmt"
	"gitlab.com/hpersh/matrix"
)

func main() {
	m := matrix.MatrixNewIdentity(4)
	fmt.Printf("%s\n\n", m)

	m = matrix.MatrixNew(3, 4)
	m.AtPut(0, 0, -10)
	m.AtPut(1, 2, 3)
	m.AtPut(2, 1, 1024)
	fmt.Printf("%s\n\n", m)
	fmt.Printf("%s\n\n", m.Transpose())

	m = matrix.MatrixNewInit(3, 4, []int{3, 4, 1, 0, 2, 5, -7, 1, 4, 8, 2, -1})
	fmt.Printf("%s\n\n", m)

	m = m.Reduce(2, 1, 0)
	fmt.Printf("%s\n\n", m)

	m = m.Reduce(1, 0, 0)
	fmt.Printf("%s\n\n", m)

	m = matrix.MatrixNewInit(3, 4, []int{3, 4, 1, 0, 2, 5, -7, 1, 4, 8, 2, -1})
	fmt.Printf("%s\n\n", m.Gaussian())
	x := m.Solve()
	fmt.Printf("x = %v\n\n", x)
}
